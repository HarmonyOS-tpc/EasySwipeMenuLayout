## EasySwipeMenuLayout

## Introduction
A sliding menu library not just for ListContainer, but all views.

### Features Supported:

- 1、Two-way sliding
- 2、Support any View
- 3、By id binding layout, more freedom

## Usage Instruction
- User can create component as EasySwipeMenuLayout at the top-level of layout file. This will add swipe functionality to this component.

```
<com.guanaj.easyswipemenulibrary.EasySwipeMenuLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/res/ohos/app"
    ohos:id="$+id:es"
    ohos:width="match_parent"
    ohos:height="match_content"
    ohos:background_element="#00ffff"
    app:contentView="$+id:content"
    app:leftMenuView="$+id:left"
    app:rightMenuView="$+id:right"
    app:canLeftSwipe="true"
    app:canRightSwipe="true">

    <DirectionalLayout
        ohos:id="$+id:left"
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:background_element="#0099cc"
        ohos:orientation="horizontal"
        ohos:padding="20vp">

        <Text
            ohos:id="$+id:left_menu"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:clickable="true"
            ohos:text_size="20vp"
            ohos:text="$string:left" />

    </DirectionalLayout>

    <DirectionalLayout
        ohos:id="$+id:content"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:background_element="#cccccc"
        ohos:orientation="vertical"
        ohos:padding="20vp">

        <Text
            ohos:id="$+id:contentText"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:text_size="20vp"
            ohos:text="$string:content" />

    </DirectionalLayout>

    <DirectionalLayout
        ohos:id="$+id:right"
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:orientation="horizontal">

        <Text
            ohos:id="$+id:right_menu"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:background_element="#00ddff"
            ohos:clickable="true"
            ohos:padding="20vp"
            ohos:text_size="20vp"
            ohos:text="$string:right_1" />

        <Text
            ohos:id="$+id:right_menu_2"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:background_element="#ff8800"
            ohos:clickable="true"
            ohos:padding="20vp"
            ohos:text_size="20vp"
            ohos:text="$string:right_2" />

    </DirectionalLayout>
</com.guanaj.easyswipemenulibrary.EasySwipeMenuLayout>
```

## Installation instruction
### Method 1:
Generate the .har package through the library and add the .har package to the libs folder.
1. Add the .har package to the lib folder.
2. Add the following code to the gradle of the entry:
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

### Method 2:

```gradle
allprojects {
    repositories {
        mavenCentral()
    }
}

dependencies {
    implementation 'io.openharmony.tpc.thirdlib:EasySwipeMenuLayout:1.0.1'
}
```