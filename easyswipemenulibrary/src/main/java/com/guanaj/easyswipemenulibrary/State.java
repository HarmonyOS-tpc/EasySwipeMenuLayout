package com.guanaj.easyswipemenulibrary;

/**
 * Created by guanaj on 2017/6/6.
 */

public enum State {

    /**
     * To maintains state of opened menu after swipe
     *
     * @LEFTOPEN - Left menu is opened
     * @RIGHTOPEN - right menu is opened
     * @CLOSE - Left and right menus are opened
     */
    LEFTOPEN,
    RIGHTOPEN,
    CLOSE,
}