/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.guanaj.easyswipemenulayout.slice;

import com.guanaj.easyswipemenulayout.ResourceTable;
import com.guanaj.easyswipemenulibrary.EasySwipeMenuLayout;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice implements Component.ScrolledListener {
    private final int LIST_ITEM_COUNT = 20;

    private ListContainer listContainer;

    private SwipeItemProvider swipeItemProvider;

    private ArrayList<String> contentList;

    private EasySwipeMenuLayout mainEasySwipeMenuLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initComponents();
        initContactData();
        initProvider();
    }

    private void initComponents() {
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
    }

    private void initProvider() {
        swipeItemProvider = new SwipeItemProvider(this, contentList);
        swipeItemProvider.getCount();
        listContainer.setItemProvider(swipeItemProvider);
        listContainer.setReboundEffect(true);
    }

    private void initContactData() {
        contentList = new ArrayList<>();
        for (int i = 0; i < LIST_ITEM_COUNT; i++) {
            contentList.add("" + i);
        }
    }

    /**
     * Provider for Listcontainer to create list with swipe components
     */
    public class SwipeItemProvider extends BaseItemProvider {
        private final int TOAST_DURATION = 1000;

        private List<String> contentList;

        private Context context;

        public SwipeItemProvider(Context context, List<String> contentList) {
            this.contentList = contentList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return contentList.size();
        }

        @Override
        public Object getItem(int position) {
            return contentList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            Component container = getComponent(position);
            return container;
        }

        private Component getComponent(int position) {
            String content = contentList.get(position);

            Component container = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_list_item,
                    null, false);

            Text contentText = (Text) container.findComponentById(ResourceTable.Id_contentText);
            contentText.setText("content " + content);

            EasySwipeMenuLayout easySwipeMenuLayout = (EasySwipeMenuLayout) container.findComponentById(
                ResourceTable.Id_es);
            mainEasySwipeMenuLayout = easySwipeMenuLayout;

            DirectionalLayout contentD = (DirectionalLayout) container.findComponentById(ResourceTable.Id_content);
            Text rightFirstText = (Text) container.findComponentById(ResourceTable.Id_right_menu);
            Text rightSecondText = (Text) container.findComponentById(ResourceTable.Id_right_menu_2);
            Text leftText = (Text) container.findComponentById(ResourceTable.Id_left_menu);

            contentD.setClickedListener(
                component -> swipeClicked(contentText.getText(), position, easySwipeMenuLayout));
            rightFirstText.setClickedListener(component -> swipeClicked("Right first", position,
                    easySwipeMenuLayout));
            rightSecondText.setClickedListener(component -> swipeClicked("Right second", position,
                    easySwipeMenuLayout));
            leftText.setClickedListener(component -> swipeClicked("left", position, easySwipeMenuLayout));

            return container;
        }

        private void swipeClicked(String content, int position, EasySwipeMenuLayout easySwipeMenuLayout) {
            ToastDialog toast = new ToastDialog(context);
            toast.setText(position + " click " + content);
            toast.setDuration(TOAST_DURATION);
            toast.show();

            easySwipeMenuLayout.resetStatus();
        }
    }

    @Override
    protected void onInactive() {
        super.onInactive();

        if (mainEasySwipeMenuLayout != null) {
            mainEasySwipeMenuLayout.resetStatus();
        }
    }

    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
        EasySwipeMenuLayout easy = (EasySwipeMenuLayout) component;
        if(easy != null) easy.resetStatus();
    }
}